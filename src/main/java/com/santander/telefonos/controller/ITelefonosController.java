package com.santander.telefonos.controller;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.santander.telefonos.entities.Telefono;

public interface ITelefonosController {
	
	@Cacheable(value = "telefonos", key = "#id", unless = "#result.followers < 1000")
	@GetMapping(value="/telefonos")
	public ResponseEntity<?> getListTelefonos(@RequestHeader(value = "DATA", required = true) String data);
	
	@Cacheable(value = "imei", key = "#imei", unless = "#result.followers < 1000")
	@GetMapping(value="/telefono/{imei}")
	public ResponseEntity<?> getTelefonosByImei(@RequestHeader(value = "DATA", required = true) String data, @PathVariable("imei") String imei);
	
	@Cacheable(value = "telefono", key = "#id", unless = "#result.followers < 1000")
	@PostMapping(value = "/telefono")
    public ResponseEntity<?> agregarTelefono(@RequestHeader(value = "DATA", required = true) String data, @RequestBody Telefono telefono);
	
	@CachePut(value = "telefono", key = "#telefono.id")
	@PutMapping(value = "/telefono/{id}")
    public ResponseEntity<?> actualizarTelefono(@RequestHeader(value = "DATA", required = true) String data, @PathVariable("id") Long id, @RequestBody Telefono telefono);
	
	@CacheEvict(value = "telefono", allEntries=true)
	@DeleteMapping(value = "/telefono/{id}")
    public ResponseEntity<?> quitarTelefono(@RequestHeader(value = "DATA", required = true) String data, @PathVariable("id") Long id);

}
