package com.santander.telefonos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.santander.telefonos.entities.Telefono;
import com.santander.telefonos.exceptions.ResourceNotFoundException;
import com.santander.telefonos.service.ITelefonosService;
import com.santander.telefonos.utilities.ValidateData;

@RestController
@RequestMapping("/crud")
public class TelefonosControllerImpl implements ITelefonosController {
	
	@Autowired
	ITelefonosService service;
	
	@Autowired
	ValidateData validate_data;

	@Override
	public ResponseEntity<?> getListTelefonos(String data) {
		
		validate_data.headers(data);
		return service.getListTelefonos(); 
	}

	@Override
	public ResponseEntity<?> getTelefonosByImei(String data, String imei) {
		try {
			validate_data.headers(data);
			Telefono telefono = service.getTelefonoByImei(imei);
			return ResponseEntity.status(HttpStatus.OK).body(telefono);
		} catch(ResourceNotFoundException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
		}
	}

	@Override
	public ResponseEntity<?> agregarTelefono(String data, Telefono telefono) {
		validate_data.headers(data);
		Telefono respuesta = service.agregaTelefono(telefono);
		if(respuesta == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No se a\\u00f1adio telefono");
		}else {
			return ResponseEntity.status(HttpStatus.OK).body(respuesta);
		}
	}

	@Override
	public ResponseEntity<?> actualizarTelefono(String data, Long id, Telefono telefono) {
		try {
			validate_data.headers(data);
			Telefono respuesta = service.actualizarTelefono(id, telefono);
			return ResponseEntity.status(HttpStatus.OK).body(respuesta);
		}catch(ResourceNotFoundException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
		}
	}

	@Override
	public ResponseEntity<?> quitarTelefono(String data, Long id) {
		try {
			validate_data.headers(data);
			Boolean respuesta = service.quitarTelefono(id);
			if(respuesta == true) {
				return ResponseEntity.status(HttpStatus.OK).body("Se elimino telefono con id: " + id);
			}else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No se elimino telefono");
			}
		} catch(ResourceNotFoundException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
		}
	}

}
