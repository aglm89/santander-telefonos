package com.santander.telefonos.exceptions;

import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {
	
	private static final Logger log = Logger.getLogger(ControllerAdvisor.class.getName());
	
	@ExceptionHandler(InvalidHeadersException.class)
    public ResponseEntity<?> HandleInvalidHeadersException( InvalidHeadersException ex, WebRequest request) {		
		log.info(ex.getMessage());
		return new ResponseEntity<>("Header no match", HttpStatus.BAD_REQUEST);
        
    }

}
