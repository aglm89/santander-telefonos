package com.santander.telefonos.exceptions;

public class InvalidHeadersException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidHeadersException(String message) {

        super(message);
    }

}
