package com.santander.telefonos.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.santander.telefonos.entities.Telefono;

@Repository
public interface ITelefonosRepository extends JpaRepository<Telefono, Long> {
	
	public Optional<Telefono> findByImei(String imei);

}
