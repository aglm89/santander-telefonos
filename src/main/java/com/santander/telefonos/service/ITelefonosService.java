package com.santander.telefonos.service;

import org.springframework.http.ResponseEntity;

import com.santander.telefonos.entities.Telefono;

public interface ITelefonosService {
	
	public ResponseEntity<?> getListTelefonos();
	
	public Telefono getTelefonoByImei(String imei);
	
	public Telefono agregaTelefono(Telefono telefono);
	
	public Telefono actualizarTelefono(Long id, Telefono telefono);
	
	public Boolean quitarTelefono(Long id);

}
