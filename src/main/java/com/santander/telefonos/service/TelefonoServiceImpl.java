package com.santander.telefonos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.santander.telefonos.entities.Telefono;
import com.santander.telefonos.exceptions.ResourceNotFoundException;
import com.santander.telefonos.repository.ITelefonosRepository;

@Service
public class TelefonoServiceImpl implements ITelefonosService {
	
	@Autowired
	ITelefonosRepository repo;

	@Override
	public ResponseEntity<?> getListTelefonos() {
		List<Telefono> listadoTelefonos = (List<Telefono>) repo.findAll();
		if(!listadoTelefonos.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK).body(listadoTelefonos);
		}else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Sin Resultados");
		}
	}

	@Override
	public Telefono getTelefonoByImei(String imei) {
		Telefono telefono = repo.findByImei(imei).orElseThrow(() -> new ResourceNotFoundException("Telefono", "id", imei));
		return telefono;
	}

	@Override
	public Telefono agregaTelefono(Telefono telefono) {
		try {
			Telefono agregatelefono = repo.save(telefono);
			return agregatelefono;
		} catch (DataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Telefono actualizarTelefono(Long id, Telefono telefono) {
		Telefono upTelefono = repo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Telefono", "id", id));
		
		upTelefono.setNombreMarca(telefono.getNombreMarca());
		upTelefono.setModelo(telefono.getModelo());
		upTelefono.setNombreCorto(telefono.getNombreCorto());
		upTelefono.setImei(telefono.getImei());
		
		upTelefono.setNumeroCelular(telefono.getNumeroCelular() == null ? null : telefono.getNumeroCelular());
		upTelefono.setEmail(telefono.getEmail() == null ? null : telefono.getEmail());
		upTelefono.setIos(telefono.getIos() == null ? false : telefono.getIos());
		
		Telefono telefonoFinal = repo.save(upTelefono);
		return telefonoFinal;
	}

	@Override
	public Boolean quitarTelefono(Long id) {
		Telefono telefono = repo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Telefono", "id", id));
		try {
			repo.delete(telefono);
			return true;
		} catch (DataAccessException e) {
			return false;
		}
	}

}
