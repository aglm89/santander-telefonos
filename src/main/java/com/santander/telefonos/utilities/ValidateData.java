package com.santander.telefonos.utilities;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.santander.telefonos.exceptions.InvalidHeadersException;

@Component
public class ValidateData {
	
	@Value("${santander.header.hash}")
	private String headerHash;
	
	public boolean headers(String data) {
		if(data == null || data.isEmpty()) {
			throw new InvalidHeadersException("Header hash not match");
		} else {
			if(data.equals(headerHash)) {
		    	return true;
		    } else {
		    	throw new InvalidHeadersException("Header hash not match");
		    }
		}
	}

}
